#include "../hybrid_generated.h"

float difflogic(float in) {
    static bool bin[8 * 64];
    static int iout[64];
    int x = (in + 1.0f) / 2.0f * 255.0f;
    for (int j = 0; j < 8; j++) {
        bin[j] = x & 1;
        x >>= 1;
    }
    apply_logic_gate_net(bin, iout, 1);
    return (iout[0] / 255.0f) * 2.0f - 1.0f;
}
