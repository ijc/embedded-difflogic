import itertools
from difflogic import LogicLayer, GroupSum, CompiledLogicNet
import torch
import torch.nn as nn
import networkx as nx
import matplotlib.pyplot as plt
import sympy
from assembled_model import AssembledLogicNet, ALL_OPERATIONS, OPERATION_CODE
from dataclasses import dataclass
from typing import Optional


@dataclass(frozen=True)
class Node:
    layer: int
    index: int
    offset: float
    operation: Optional[str] = None
    input: bool = False
    output: frozenset = frozenset({})


def make_graph(model):
    graph = nx.MultiDiGraph()
    nodes = [[]]
    for i, layer in enumerate(layer for layer in model.model if isinstance(layer, LogicLayer)):
        if i == 0:
            for j in range(layer.in_dim):
                node = Node(-1, j, -layer.in_dim / 2, input=True)
                nodes[0].append(node)
                graph.add_node(node)
        layer_nodes = []
        a = layer.indices[0]
        b = layer.indices[1]
        op = layer.weights.argmax(1)
        for j, (a, b, op) in enumerate(zip(layer.indices[0], layer.indices[1], layer.weights.argmax(1))):
            op = ALL_OPERATIONS[op]
            node = Node(i, j, -layer.out_dim / 2, op, output=frozenset({j}) if i == len(model.model) - 2 else frozenset({}))
            graph.add_edge(nodes[i][a], node, operand=0)
            graph.add_edge(nodes[i][b], node, operand=1)
            layer_nodes.append(node)
            graph.add_node(node)
        nodes.append(layer_nodes)
    return graph


def draw_network(graph):
    plt.figure(figsize=((max(node.layer for node in graph.nodes) + 1) * 2, (max(node.index for node in graph.nodes) + 1) / 2))
    get_pos = lambda node: [(node.layer + 2)**2, node.index + node.offset]
    nx.draw(
        graph,
        pos = {node: get_pos(node) for node in graph.nodes},
        arrows=True, with_labels=True, node_size=0, font_size=8,
        labels={node: f"in{node.index}" if node.input else node.operation for node in graph.nodes},
        node_color="black", font_weight="bold", bbox={"boxstyle": "square", "facecolor": "white"},
        edge_color=[{None: "black", 0: "tab:blue", 1: "tab:orange"}[operand] for src, dst, operand in graph.edges.data("operand")]
    )
    plt.show()
