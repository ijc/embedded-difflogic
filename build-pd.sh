#!/bin/sh
if uname -a | grep bela; then
    FLAGS=-I/usr/local/include/libpd
else
    FLAGS=$(pkg-config --cflags pd)
fi
mkdir -p difflogic~ &&
gcc -fPIC pd_difflogic~.c $FLAGS -O3 -shared -o difflogic~/difflogic~.pd_linux
