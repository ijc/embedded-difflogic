#include <stdio.h>
#include <stdbool.h>
#include <dlfcn.h>

int main() {
    // $ gcc hybrid_generated.c -shared -o hybrid_ident_8bit.so
    void *handle = dlopen("./hybrid_ident_8bit.so", RTLD_LAZY);
    if (handle == NULL) {
        fprintf(stderr, "Failed to load library: %s\n", dlerror());
        return 1;
    }

    void (*apply_logic_gate_net)(bool const *inp, int *out, size_t len);
    apply_logic_gate_net = dlsym(handle, "apply_logic_gate_net");
    if (apply_logic_gate_net == NULL) {
        fprintf(stderr, "Failed to load symbol: %s\n", dlerror());
        return 1;
    }

    static bool bin[8 * 64];
    static int iout[64];
    int batch_size = 64;

    for (int i = 0; i < 256; i += batch_size) {
        for (int j = 0; j < batch_size; j++) {
            int x = i + j;
            for (int k = 0; k < 8; k++) {
                bin[j * 8 + k] = x & 1;
                x >>= 1;
            }
        }
        apply_logic_gate_net(bin, iout, 1);
        for (int j = 0; j < batch_size; j++) {
            printf("%d -> %d\n", i + j, iout[j]);
        }
    }

    dlclose(handle);
    return 0;
}
