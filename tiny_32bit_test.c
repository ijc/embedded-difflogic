#include <stdio.h>
#include "tiny_32bit.c"

int main() {
    for (int i = 0; i < 4; i++) {
        printf("%d -> %d\n", i, apply_logic_gate_net(i));
    }
    return 0;
}
