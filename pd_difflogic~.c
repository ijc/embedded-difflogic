#include <stdbool.h>
#include <dlfcn.h>
#include "m_pd.h"

// Message version
static t_class *difflogic_class;

typedef struct _difflogic {
    t_object x_obj;
    t_outlet *f_out;
    void *model_handle;
    void (*model_fn)(bool const *inp, int *out, size_t len);
} t_difflogic;

void *difflogic_new(t_symbol *path) {
    post("Loading model at path: %s", path->s_name);
    void *handle = dlopen(path->s_name, RTLD_LAZY);
    if (handle == NULL) {
        post("Failed to load model: %s", dlerror());
        return NULL;
    }

    void (*apply_logic_gate_net)(bool const *inp, int *out, size_t len);
    apply_logic_gate_net = dlsym(handle, "apply_logic_gate_net");
    if (apply_logic_gate_net == NULL) {
        post("Failed to load symbol: %s", dlerror());
        dlclose(handle);
        return NULL;
    }

    t_difflogic *self = (t_difflogic *)pd_new(difflogic_class);
    self->f_out = outlet_new(&self->x_obj, &s_float);
    self->model_handle = handle;
    self->model_fn = apply_logic_gate_net;
    return self;
}

void difflogic_float(t_difflogic *self, t_floatarg f) {
    static bool bin[8 * 64];
    static int iout[64];

    int x = (f + 1.0f) / 2.0f * 255.0f;
    for (int j = 0; j < 8; j++) {
        bin[j] = x & 1;
        x >>= 1;
    }
    self->model_fn(bin, iout, 1);
    float out = (iout[0] / 255.0f) * 2.0f - 1.0f;
    outlet_float(self->f_out, out);
}

void difflogic_free(t_difflogic *self) {
     dlclose(self->model_handle);
}

// Signal version
static t_class *difflogic_tilde_class;

typedef struct _difflogic_tilde {
    t_object x_obj;
    t_float f;
    t_outlet *x_out;
    void *model_handle;
    void (*model_fn)(bool const *inp, int *out, size_t len);
} t_difflogic_tilde;

t_int *difflogic_tilde_perform(t_int *w) {
    t_difflogic_tilde *self = (t_difflogic_tilde *)(w[1]);
    t_sample *in = (t_sample *)(w[2]);
    t_sample *out = (t_sample *)(w[3]);
    int n = (int)(w[4]);

    static bool bin[8 * 64];
    static int iout[64];
    for (; n > 0; n -= 64) {
        int batch_size = n < 64 ? n : 64;
        for (int i = 0; i < batch_size; i++) {
            int x = (*in++ + 1.0f) / 2.0f * 255.0f;
            for (int j = 0; j < 8; j++) {
                bin[i * 8 + j] = x & 1;
                x >>= 1;
            }
        }
        self->model_fn(bin, iout, 1);
        for (int i = 0; i < batch_size; i++) {
            *out++ = (iout[i] / 255.0f) * 2.0f - 1.0f;
        }
    }

    return w + 5;
}

void difflogic_tilde_dsp(t_difflogic_tilde *self, t_signal **sp) {
    dsp_add(difflogic_tilde_perform, 4, self, sp[0]->s_vec, sp[1]->s_vec, sp[0]->s_n);
}

void *difflogic_tilde_new(t_symbol *path) {
    post("Loading model at path: %s", path->s_name);
    void *handle = dlopen(path->s_name, RTLD_LAZY);
    if (handle == NULL) {
        post("Failed to load model: %s", dlerror());
        return NULL;
    }

    void (*apply_logic_gate_net)(bool const *inp, int *out, size_t len);
    apply_logic_gate_net = dlsym(handle, "apply_logic_gate_net");
    if (apply_logic_gate_net == NULL) {
        post("Failed to load symbol: %s", dlerror());
        dlclose(handle);
        return NULL;
    }

    t_difflogic_tilde *self = (t_difflogic_tilde *)pd_new(difflogic_tilde_class);
    self->x_out = outlet_new(&self->x_obj, &s_signal);
    self->model_handle = handle;
    self->model_fn = apply_logic_gate_net;
    return self;
}

void difflogic_tilde_free(t_difflogic_tilde *self) {
    outlet_free(self->x_out);
    dlclose(self->model_handle);
}

void difflogic_tilde_setup() {
    difflogic_class = class_new(
        gensym("difflogic"),
        (t_newmethod)difflogic_new,
        (t_method)difflogic_free,
        sizeof(t_difflogic),
        CLASS_DEFAULT,
        A_DEFSYMBOL,
        0
    );
    class_addfloat(difflogic_class, difflogic_float);

    difflogic_tilde_class = class_new(
        gensym("difflogic~"),
        (t_newmethod)difflogic_tilde_new,
        (t_method)difflogic_tilde_free,
        sizeof(t_difflogic_tilde),
        CLASS_DEFAULT,
        A_DEFSYMBOL,
        0
    );
    class_addmethod(difflogic_tilde_class,
        (t_method)difflogic_tilde_dsp, gensym("dsp"), A_CANT, 0);
    CLASS_MAINSIGNALIN(difflogic_tilde_class, t_difflogic_tilde, f);
}
