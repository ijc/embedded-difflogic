#!/bin/sh
OUT=${1%.c}.wasm
emcc -Oz --no-entry -sSUPPORT_ERRNO=0 -sSUPPORT_LONGJMP=0 -sEXPORTED_FUNCTIONS=_logic_gate_net,_inp,_out -o $OUT $@
