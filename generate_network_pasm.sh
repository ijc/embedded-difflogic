#!/bin/sh
# TODO this is super hacky, my condolences to anyone who has the misfortune of gazing upon this
# basically the goal is to get some assembly for a difflogic network that I can include in the midst of some existing PRU assembly
# which already handles setup & communication with the CPU
# (yes I know linkers & inline assembly are a thing)
clpru --c99 -I/usr/share/ti/cgt-pru/include/ -D=PRU hybrid_32bit.c -k -O3 -v1 &&
lnkpru hybrid_32bit.obj -e apply_logic_gate_net -o hybrid_32bit.bin &&
dispru hybrid_32bit.bin | tail -n+5 | sed -E 's/^.{27}//' | tr $ _ | head -n-1 > test-pru/network.p.inc &&
touch test-pru/pru_gpio.p # ensure this gets reassembled on next build
