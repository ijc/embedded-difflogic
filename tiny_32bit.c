#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>

static inline void logic_gate_net(int const *inp, int *out) {
    const int v0 = inp[0];
    const int v1 = inp[1];
	out[0] = v0 & v1;
	out[1] = v0 | v1;
	out[2] = v1;
}

// void apply_logic_gate_net (bool const *inp, int *out, size_t len) {
int apply_logic_gate_net(unsigned int inp) {
    // int *inp_temp = malloc(2*sizeof(int));
    // int *out_temp = malloc(3*sizeof(int));
    // int *out_temp_o = malloc(2*sizeof(int));
    static int inp_temp[2];
    static int out_temp[3];
    static int out_temp_o[2];
    
    // for(size_t i = 0; i < len; ++i) {
    
        // Converting the bool array into a bitpacked array
        for(size_t d = 0; d < 2; ++d) {
            // int res = 0;
            // for(size_t b = 0; b < 32; ++b) {
            //     res <<= 1;
            //     res += !!(inp[i * 2 * 32 + (32 - b - 1) * 2 + d]);
            // }
            // inp_temp[d] = res;
            inp_temp[d] = !!(inp & (1 << d));
        }
    
        // Applying the logic gate net
        logic_gate_net(inp_temp, out_temp);
        
        // GroupSum of the results via logic gate networks
        // for(size_t c = 0; c < 1; ++c) {  // for each class
            // Initialize the output bits
            for(size_t d = 0; d < 2; ++d) {
                out_temp_o[d] = 0;
            }
            
            // Apply the adder logic gate network
            for(size_t a = 0; a < 3; ++a) {
                int carry = out_temp[a]; // [c * 3 + a];
                int out_temp_o_d;
                for(int d = 2 - 1; d >= 0; --d) {
                    out_temp_o_d  = out_temp_o[d];
                    out_temp_o[d] = carry ^ out_temp_o_d;
                    carry         = carry & out_temp_o_d;
                }
            }
            
            // Unpack the result bits
            // for(size_t b = 0; b < 32; ++b) {
                const int bit_mask = 1; // << b;
                int res = 0;
                for(size_t d = 0; d < 2; ++d) {
                    res <<= 1;
                    res += !!(out_temp_o[d] & bit_mask);
                }
                // out[(i * 32 + b) * 1 + c] = res;
                return res;
            // }
        // }
    // }
    // free(inp_temp);
    // free(out_temp);
    // free(out_temp_o);
}
