import torch
import math
from difflogic import LogicLayer, GroupSum
import numpy as np
import numpy.typing
import time
from typing import Union


ALL_OPERATIONS = [
    "zero",
    "and",
    "not_implies",
    "a",
    "not_implied_by",
    "b",
    "xor",
    "or",
    "not_or",
    "not_xor",
    "not_b",
    "implied_by",
    "not_a",
    "implies",
    "not_and",
    "one",
]

OPERATION_CODE = {
    # operation: (uses_a, uses_b, source_code)
    # zero-argument operations
    "zero": (False, False, "LDI R4, 0"),
    "one": (False, False, "LDI R4, 0\nNOT R4, R4"),
    # one-argument operations
    "a": (True, False, "MOV R4, R2"),
    "b": (False, True, "MOV R4, R3"),
    "not_a": (True, False, "NOT R4, R2"),
    "not_b": (False, True, "NOT R4, R3"),
    # two-argument operations
    "and": (True, True, "AND R4, R2, R3"),
    "not_implies": (True, True, "NOT R3, R3\nAND R4, R2, R3"),
    "not_implied_by": (True, True, "NOT R2, R2\nAND R4, R2, R3"),
    "xor": (True, True, "XOR R4, R2, R3"),
    "or": (True, True, "OR R4, R2, R3"),
    "not_or": (True, True, "OR R4, R2, R3\nNOT R4, R4"),
    "not_xor": (True, True, "XOR R4, R2, R3\nNOT R4, R4"),
    "implied_by": (True, True, "NOT R3, R3\nOR R4, R2, R3"),
    "implies": (True, True, "NOT R2, R2\nOR R4, R2, R3"),
    "not_and": (True, True, "AND R4, R2, R3\nNOT R4, R4"),
}

class AssembledLogicNet(torch.nn.Module):
    def __init__(
            self,
            model: torch.nn.Sequential,
            # device='cpu', # Assumed to be PRU for now
            num_bits=8,
            verbose=False,
    ):
        super().__init__()
        self.model = model
        self.num_bits = num_bits

        assert num_bits == 8 # TODO
        # assert num_bits in [8, 16, 32]

        if self.model is not None:
            layers = []

            self.num_inputs = None

            assert isinstance(self.model[-1], GroupSum), 'The last layer of the model must be GroupSum, but it is {} / {}' \
                                                         ' instead.'.format(type(self.model[-1]), self.model[-1])
            self.num_classes = self.model[-1].k

            first = True
            for layer in self.model:
                if isinstance(layer, LogicLayer):
                    if first:
                        self.num_inputs = layer.in_dim
                        first = False
                    self.num_out_per_class = layer.out_dim // self.num_classes
                    layers.append((layer.indices[0], layer.indices[1], layer.weights.argmax(1)))
                elif isinstance(layer, torch.nn.Flatten):
                    if verbose:
                        print('Skipping torch.nn.Flatten layer ({}).'.format(type(layer)))
                elif isinstance(layer, GroupSum):
                    if verbose:
                        print('Skipping GroupSum layer ({}).'.format(type(layer)))
                else:
                    assert False, 'Error: layer {} / {} unknown.'.format(type(layer), layer)

            self.layers = layers

            if verbose:
                print('`layers` created and has {} layers.'.format(len(layers)))

        self.lib_fn = None

    def get_layer_code(self, layer_a, layer_b, layer_op, layer_id, prefix_sums):
        code = []
        code.append(f"LDI R5, {self.num_inputs + prefix_sums[layer_id]}")
        for var_id, (gate_a, gate_b, gate_op) in enumerate(zip(layer_a, layer_b, layer_op)):
            operation_name = ALL_OPERATIONS[gate_op]
            uses_a, uses_b, implementation = OPERATION_CODE[operation_name]
            if layer_id == 0:
                code.append(f"// layer[{layer_id}][{var_id}] = {operation_name}(input[{gate_a}], input[{gate_b}])")
                if uses_a: code.append(f"LBBO R2.b0, R0, {gate_a}, 1")
                if uses_b: code.append(f"LBBO R3.b0, R0, {gate_b}, 1")
            else:
                code.append(f"// layer[{layer_id}][{var_id}] = {operation_name}(layer[{layer_id-1}][{gate_a}], layer[{layer_id-1}][{gate_b}])")
                if uses_a: code.append(f"LBBO R2.b0, R1, {prefix_sums[layer_id - 1] + gate_a}, 1")
                if uses_b: code.append(f"LBBO R3.b0, R1, {prefix_sums[layer_id - 1] + gate_b}, 1")
            code.append(implementation)
            code.append(f"SBBO R4.b0, R5, {var_id}, 1")
            code.append(f"")
        return code

    def get_asm(self):
        prefix_sums = [0]
        cur_count = 0
        for layer_a, layer_b, layer_op in self.layers[:-1]:
            cur_count += len(layer_a)
            prefix_sums.append(cur_count)

        code = [f"""
LDI R0, 0 // inp
LDI R1, {self.num_inputs} // v

// R14 has the input
// R2 has the count
// R3 has the memory offset
// R4 has the bit
LDI R2, 0
LDI R3, 0
INPUT_BITPACK:
AND R4, R14, 1
SBBO R4, R0, R3, 1
ADD R2, R2, 1
ADD R3, R3, 1 // TODO: consolidate with R2 if we stick with 8-bit batches
LSR R14, R14, 1
QBGT INPUT_BITPACK, R2, {self.num_inputs}
"""]

        for layer_id, (layer_a, layer_b, layer_op) in enumerate(self.layers):
            code.extend(self.get_layer_code(layer_a, layer_b, layer_op, layer_id, prefix_sums))

        # num_neurons_ll = self.layers[-1][0].shape[0]
        # log2_of_num_neurons_per_class_ll = math.ceil(math.log2(num_neurons_ll / self.num_classes + 1))

        code.append(f"""
// just handling one input & output for the moment...
LDI R0, {self.num_inputs + cur_count} // out_temp
// R1 has the count
// R2 has the memory offset
// R3 has the output value
// R14 has the running total
LDI R1, 0
LDI R2, 0
LDI R14, 0
GROUPSUM:
LBBO R3.b0, R0, R2, 1
AND R3.b0, R3.b0, 1
ADD R14, R14, R3.b0
ADD R1, R1, 1
ADD R2, R2, 1 // TODO: consolidate with R1 if we stick with 8-bit batches
QBGT GROUPSUM, R1, {self.layers[-1][0].shape[0] // self.num_classes}
""")
        return "\n".join(code)
