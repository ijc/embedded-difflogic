#include <iostream>
#include <vector>
#include "hybrid_generated.h"

class DifflogicNet {
    public:
    // TODO: Options for configuring network input/output representation.
    // Shall we "bake in" a given difflogic network when compiling the wrapper,
    // or shall we load a pre-compiled difflogic network dynamically?
    DifflogicNet();
    std::vector<double> process(const std::vector<double> &input);
};

DifflogicNet::DifflogicNet() {
    // TODO: Allocate memory here to avoid repeated `malloc()`/`free()` in `apply_logic_gate_net()`
    // (This will require modifying the output of difflogic's C exporter or writing a custom exporter.)
}

// TODO: Offer non-allocating version; consider thread-safe version.
std::vector<double> DifflogicNet::process(const std::vector<double> &input) {
    // Sample-oriented; inputs/outputs are in range [-1, 1].
    // TODO: Make number of inputs/outputs configurable (or derived from code generated from network).
    const int max_batch_size = 64;
    static bool bin[8 * max_batch_size];
    static int iout[max_batch_size];
    std::vector<double> output;
    int n = input.size();
    int input_index = 0;
    for (; n > 0; n -= max_batch_size) {
        int batch_size = n < max_batch_size ? n : max_batch_size;
        for (int i = 0; i < batch_size; i++) {
            int x = (input[input_index++] + 1.0f) / 2.0f * 255.0f;
            for (int j = 0; j < 8; j++) {
                bin[i * 8 + j] = x & 1;
                x >>= 1;
            }
        }
        apply_logic_gate_net(bin, iout, 1);
        for (int i = 0; i < batch_size; i++) {
            output.push_back((iout[i] / 255.0f) * 2.0f - 1.0f);
        }
    }
    return output;
}

int main() {
    DifflogicNet net;
    std::vector<double> values{-0.8, 0.3, 0.9, 0.42};
    auto output = net.process(values);
    for (auto out : output) {
        std::cout << out << " ";
    }
    std::cout << std::endl;
    return 0;
}
