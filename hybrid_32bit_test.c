#include <stdio.h>
#include "hybrid_32bit.c"

int main() {
    for (int i = 0; i < 1; i++) {
        printf("%d -> %d\n", i, apply_logic_gate_net(i));
    }

    for (int i = 0; i < 8; i++) {
	 	printf("inp_temp[%d] = %u\n", i, inp_temp[i]);
	}
	
	for (int i = 0; i < 240; i++) {
	 	printf("v[%d] = %u\n", i, v[i]);
	}
    return 0;
}
