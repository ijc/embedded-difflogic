// PluginDifflogic.hpp
// Ian Clester (ijc@ijc8.me)

#pragma once

#include "SC_PlugIn.hpp"

namespace Difflogic {

class Difflogic : public SCUnit {
public:
    Difflogic();

    // Destructor
    // ~Difflogic();

private:
    // Calc function
    void next(int nSamples);

    // Member variables
};

} // namespace Difflogic
