// PluginDifflogic.cpp
// Ian Clester (ijc@ijc8.me)

#include "SC_PlugIn.hpp"
#include "Difflogic.hpp"
#include "../../../hybrid_generated.h"

static InterfaceTable* ft;

namespace Difflogic {

Difflogic::Difflogic() {
    mCalcFunc = make_calc_function<Difflogic, &Difflogic::next>();
    next(1);
}

void Difflogic::next(int nSamples) {

    // Audio rate input
    const float* input = in(0);

    // Output buffer
    float* outbuf = out(0);

    static bool bin[8 * 64];
    static int iout[64];
    for (int j = 0; j < nSamples;) {
        int remaining = nSamples - j;
        int batch_size = remaining < 64 ? remaining : 64;
        for (int i = 0; i < batch_size; i++) {
            int x = (input[i + j] + 1.0f) / 2.0f * 255.0f;
            for (int j = 0; j < 8; j++) {
                bin[i * 8 + j] = x & 1;
                x >>= 1;
            }
        }
        apply_logic_gate_net(bin, iout, 1);
        for (int i = 0; i < batch_size; i++) {
            outbuf[i + j] = (iout[i] / 255.0f) * 2.0f - 1.0f;
        }
        j += batch_size;
    }
}

} // namespace Difflogic

PluginLoad(DifflogicUGens) {
    // Plugin magic
    ft = inTable;
    registerUnit<Difflogic::Difflogic>(ft, "Difflogic", false);
}
