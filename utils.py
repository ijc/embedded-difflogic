import torch
import torch.nn as nn
from tqdm import tqdm
import matplotlib.pyplot as plt


class UnaryInput(nn.Module):
    # AKA Thermometer Encoding
    # NOTE: Corresponding "unary output" is just difflogic's GroupSum
    def __init__(self, num_options):
        super().__init__()
        self.num_options = num_options

    def forward(self, X):
        Xgs = torch.zeros((X.size(0), self.num_options), dtype=X.dtype, device=X.device)
        for x, xgs in zip(X, Xgs):
            xgs[:int(x)] = 1
        return Xgs


class BinaryInput(nn.Module):
    def __init__(self, num_bits):
        super().__init__()
        self.masks = (1 << torch.arange(num_bits))[None, :]

    def forward(self, x):
        # TODO consider differentiability
        # TODO consider scaling:
        #   should we scale the input by 2**(-num_bits) so the caller doesn't have to?
        #   this would assume an original input range of [0,1).
        # convert int to bits
        # X = [[(x & (1 << i)) >> i for i in range(self.num_bits)] for x in X]
        return (x.to(int)[:, None] & self.masks).sign()


class BinaryOutput(nn.Module):
    def __init__(self, num_bits):
        super().__init__()
        self.factors = 2 ** torch.arange(num_bits, dtype=float)

    def forward(self, x):
        return (x * self.factors).sum(axis=1)


def train(dataloader, model, loss_fn, optimizer, device=None, print_period=1):
    size = len(dataloader.dataset)
    model.train()
    for batch, (X, y) in enumerate(dataloader):
        if device:
            X, y = X.to(device), y.to(device)

        # Compute prediction error
        pred = model(X)
        loss = loss_fn(pred, y)

        # Backpropagation
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

        if batch % print_period == 0:
            loss, current = loss.item(), (batch + 1) * len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")


def test(model, inputs, outputs, loss_fn):
    model.eval()
    pred = torch.zeros(outputs.size(0), dtype=torch.float)
    with torch.no_grad():
        for i, x in tqdm(enumerate(inputs)):
            y = model(torch.Tensor([x]))
            pred[i] = y
    loss = loss_fn(pred, outputs.float())
    print(f"Loss: {loss.item()}")
    return pred


def plot_model(model, inputs, outputs, loss_fn):
    model.train()
    train_output = model(inputs).detach()
    model.eval()
    test_output = model(inputs).detach()

    print(f"Real-valued network loss: {loss_fn(outputs, train_output)}")
    print(f"Discrete network loss:    {loss_fn(outputs, test_output)}")

    plt.plot(outputs.cpu(), label="ground truth")
    plt.plot(train_output.cpu(), label="training")
    plt.plot(test_output.cpu(), label="testing")
    plt.legend()
