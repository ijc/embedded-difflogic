enum OPERATIONS {
    ZERO,
    AND,
    NOT_IMPLIES,
    A,
    NOT_IMPLIED_BY,
    B,
    XOR,
    OR,
    NOT_OR,
    NOT_XOR,
    NOT_B,
    IMPLIED_BY,
    NOT_A,
    IMPLIES,
    NOT_AND,
    ONE,
};

// TODO: consider re-allocating as necessary
#define MAX_INPUTS 32
#define MAX_GATES 65536

int num_inputs = 0;
int num_gates = 0;
// TODO: include `float process(float)` here and make `num_outputs` configurable
int data[MAX_INPUTS + MAX_GATES];

struct {
	int input_a;
	int input_b;
	int op;
} network[MAX_GATES];

void setup(int new_num_inputs, int new_num_gates) {
    num_inputs = new_num_inputs;
    num_gates = new_num_gates;
}

static inline int do_op(int op, int a, int b) {
    switch (op) {
    case ZERO:
        return 0;
    case AND:
        return a & b;
    case NOT_IMPLIES:
        return a & ~b;
    case A:
        return a;
    case NOT_IMPLIED_BY:
        return b & ~a;
    case B:
        return b;
    case XOR:
        return a ^ b;
    case OR:
        return a | b;
    case NOT_OR:
        return ~(a | b);
    case NOT_XOR:
        return ~(a ^ b);
    case NOT_B:
        return ~b;
    case IMPLIED_BY:
        return ~b | a;
    case NOT_A:
        return ~a;
    case IMPLIES:
        return ~a | b;
    case NOT_AND:
        return ~(a & b);
    case ONE:
        return ~0;
    };
}

void logic_gate_net() {
	for (int i = 0; i < num_gates; i++) {
		data[num_inputs + i] = do_op(network[i].op, data[network[i].input_a], data[network[i].input_b]);
	}
}
