import { useEffect, useState } from 'react'
import './App.css'
// @ts-expect-error importing untyped module
import BelaData from "./bela/BelaData"

const context = new AudioContext({ sampleRate: 8000 })
console.log(context.state, context.sampleRate)
let node: AudioWorkletNode
const gainNode = new GainNode(context)

const OP_INPUTS = {
    // zero-argument operations
    zero: [false, false],
    one: [false, false],
    // one-argument operations
    a: [true, false],
    b: [false, true],
    not_a: [true, false],
    not_b: [false, true],
    // two-argument operations
    and: [true, true],
    not_implies: [true, true],
    not_implied_by: [true, true],
    xor: [true, true],
    or: [true, true],
    not_or: [true, true],
    not_xor: [true, true],
    implied_by: [true, true],
    implies: [true, true],
    not_and: [true, true],
}

type Operation = keyof typeof OP_INPUTS
const OPS = [
    "zero",
    "and",
    "not_implies",
    "a",
    "not_implied_by",
    "b",
    "xor",
    "or",
    "not_or",
    "not_xor",
    "not_b",
    "implied_by",
    "not_a",
    "implies",
    "not_and",
    "one",
] as Operation[]


const wireLength = 40
const notRadius = 10
const strokeWidth = 8
const Buffer = ({ not }: { not: boolean }) => {
    const height = 100
    const width = height * Math.sqrt(3) / 2
    const totalWidth = width + wireLength*2
    const outWireStartX = wireLength + width + (not ? notRadius*2 : 0)
    // return <svg viewBox={`0 0 ${totalWidth} ${height}`} strokeWidth={strokeWidth} style={{ width: "100%", height: "100%", aspectRatio: "1 / 1" }}>
    return <g strokeWidth={strokeWidth} transform={`scale(${1/totalWidth}) translate(0, ${-height/2})`}>
        <line x1="0" y1={height/2} x2={wireLength} y2={height/2} />
        <polygon points={`${wireLength},0 ${wireLength},${height} ${wireLength + width},${height/2}`} fill="none" />
        {not && <circle cx={wireLength + width + notRadius} cy={height/2} r={notRadius} fill="none" />}
        <line x1={outWireStartX} y1={height/2} x2={totalWidth} y2={height/2} />
    </g>
}

const And = ({ not_a, not_b, not }: { not_a: boolean, not_b: boolean, not: boolean }) => {
    const height = 90
    const width = 50
    const curveWidth = 37
    const outWireStartX = wireLength + width + curveWidth + (not ? notRadius*2 : 0)
    const totalWidth = width + curveWidth + wireLength * 2
    return <g strokeWidth={strokeWidth} transform={`scale(${1/totalWidth}) translate(0, ${-height/2})`}>
        <line x1="0" y1={height/4} x2={wireLength - (not_a ? notRadius*2 : 0)} y2={height/4} />
        <line x1="0" y1={3*height/4} x2={wireLength - (not_b ? notRadius*2 : 0)} y2={3*height/4} />
        {not_a && <circle cx={wireLength - notRadius} cy={height/4} r={notRadius} fill="none" />}
        {not_b && <circle cx={wireLength - notRadius} cy={3*height/4} r={notRadius} fill="none" />}
        <path d={`
            M ${wireLength} 0
            h ${width}
            c 50 0, 50 ${height}, 0 ${height}
            h -${width}
            Z`}
            fill="none"
        />
        {not && <circle cx={wireLength + width + curveWidth + notRadius} cy={height/2} r={notRadius} fill="none" />}
        <line x1={outWireStartX} y1={height/2} x2={totalWidth} y2={height/2} />
    </g>
}

const Or = ({ xor, not_a, not_b, not }: { xor: boolean, not_a: boolean, not_b: boolean, not: boolean }) => {
    const height = 90
    const startCurveWidth = 11
    const width = 50
    const curveWidth = 37
    const outWireStartX = wireLength - startCurveWidth + width + curveWidth + (not ? notRadius*2 : 0)
    const totalWidth = width + curveWidth + wireLength * 2
    const xorOffset = 13
    return <g strokeWidth={strokeWidth} transform={`scale(${1/totalWidth}) translate(0, ${-height/2})`}>
        <line x1="0" y1={height/4} x2={wireLength - (not_a ? notRadius*2 : 0)} y2={height/4} />
        <line x1="0" y1={3*height/4} x2={wireLength - (not_b ? notRadius*2 : 0)} y2={3*height/4} />
        {not_a && <circle cx={wireLength - notRadius} cy={height/4} r={notRadius} fill="none" />}
        {not_b && <circle cx={wireLength - notRadius} cy={3*height/4} r={notRadius} fill="none" />}
        {xor && <path d={`
            M ${wireLength - xorOffset - startCurveWidth} 0
            c 20 20, 20 ${height - 20}, 0 ${height}
            `} 
            fill="none"
        />}
        <path d={`
            M ${wireLength - startCurveWidth} 0
            c 50 0, ${width + curveWidth} ${height/2 - 10}, ${width + curveWidth} ${height/2}
            c 0 10, ${50 -(width + curveWidth)} ${height/2}, ${-(width + curveWidth)} ${height/2}
            c 20 -20, 20 ${-height + 20}, 0 ${-height}`}
            fill="none"
        />
        {not && <circle cx={wireLength - startCurveWidth + width + curveWidth + notRadius} cy={height/2} r={notRadius} fill="none" />}
        <line x1={outWireStartX} y1={height/2} x2={totalWidth} y2={height/2} />
    </g>
}

const Constant = ({ value }: { value: number }) => {
    const height = 100
    return <g strokeWidth={strokeWidth*3/5} transform={`scale(${1/100}) translate(0, ${-100/2})`}>
        <text
            style={{
                font: "60px monospace",
                userSelect: "none",
                WebkitUserSelect: "none", // just for safari...
                pointerEvents: "none"
            }}
            x={50} y={70} textAnchor="middle" strokeWidth={1}>
            {value}
        </text>
        <line x1={75} y1={height/2} x2={height} y2={height/2} />
    </g>
}

const OP_ICONS = {
    zero: <Constant value={0} />,
    and: <And not_a={false} not_b={false} not={false} />,
    not_implies: <And not_a={false} not_b={true} not={false} />,
    a: <Buffer not={false} />,
    not_implied_by: <And not_a={true} not_b={false} not={false} />,
    b: <Buffer not={false} />,
    xor: <Or xor={true} not_a={false} not_b={false} not={false} />,
    or: <Or xor={false} not_a={false} not_b={false} not={false} />,
    not_or: <Or xor={false} not_a={false} not_b={false} not={true} />,
    not_xor: <Or xor={true} not_a={false} not_b={false} not={true} />,
    not_b: <Buffer not={true} />,
    implied_by: <Or xor={false} not_a={false} not_b={true} not={false} />,
    not_a: <Buffer not={true} />,
    implies: <Or xor={false} not_a={true} not_b={false} not={false} />,
    not_and: <And not_a={false} not_b={false} not={true} />,
    one: <Constant value={1} />,
}

let counts: Uint8Array | null = null

async function setup() {
    document.removeEventListener("click", setup)
    await context.resume()
    await context.audioWorklet.addModule("worklet.js")
    console.log("added module")
    node = new AudioWorkletNode(context, "custom-processor")
    node.connect(gainNode)
    gainNode.connect(context.destination)
    console.log("created and connected node")
    const binary = await (await fetch("interpreter.wasm")).arrayBuffer()
    node.port.postMessage({ cmd: "loadModule", binary })
    node.port.postMessage({ cmd: "loadNetwork", network: NETWORK })
    node.port.postMessage({ cmd: "play" })
    node.port.onmessage = message => {
        counts = message.data
    }
}

// if (context.state === "suspended") {
//     const resume = async () => {
//         document.removeEventListener("click", resume)
//         console.log("click", context.state)
//         await context.resume()
//         setup()
//     }
//     document.addEventListener("click", resume)
// } else {
//     setup()
// }

document.addEventListener("click", setup)

type Network = [keyof typeof OP_INPUTS, number, number][][]

// 14-bit-input network:
// let NETWORK: [string, number, number][][] = [[["zero", 10, 12], ["not_b", 8, 4], ["one", 7, 8], ["zero", 7, 9], ["one", 5, 12], ["xor", 3, 3], ["and", 11, 5], ["a", 6, 10], ["one", 5, 2], ["a", 4, 1], ["not_and", 2, 13], ["not_a", 6, 12], ["not_or", 6, 2], ["zero", 1, 7], ["one", 9, 5], ["not_b", 7, 11], ["b", 12, 5], ["a", 11, 12], ["not_implied_by", 6, 13], ["xor", 0, 11], ["not_b", 8, 13], ["b", 9, 11], ["b", 10, 13], ["zero", 1, 10], ["not_and", 4, 3], ["zero", 10, 8], ["zero", 9, 1], ["not_a", 6, 0], ["b", 0, 13], ["one", 2, 8], ["and", 4, 9], ["xor", 3, 0]], [["a", 14, 3], ["not_implied_by", 26, 5], ["not_b", 31, 26], ["not_a", 16, 21], ["not_implied_by", 7, 11], ["and", 28, 18], ["implied_by", 19, 30], ["b", 27, 22], ["not_or", 12, 10], ["one", 14, 11], ["b", 8, 22], ["zero", 13, 25], ["not_implied_by", 6, 0], ["not_b", 25, 24], ["not_or", 15, 1], ["not_a", 27, 2], ["not_implies", 9, 21], ["not_implied_by", 31, 19], ["one", 9, 23], ["not_a", 10, 29], ["not_and", 7, 1], ["b", 0, 17], ["not_and", 30, 28], ["not_implied_by", 6, 2], ["not_and", 8, 12], ["zero", 13, 3], ["not_a", 18, 17], ["not_a", 15, 20], ["a", 5, 23], ["not_implied_by", 4, 16], ["b", 4, 29], ["not_implies", 24, 20]], [["a", 26, 2], ["b", 5, 25], ["not_and", 9, 18], ["not_a", 22, 11], ["or", 1, 14], ["one", 29, 29], ["not_xor", 13, 11], ["not_b", 18, 20], ["xor", 3, 9], ["and", 7, 31], ["not_or", 23, 0], ["xor", 17, 31], ["zero", 8, 30], ["or", 13, 0], ["not_b", 3, 21], ["or", 5, 23], ["and", 17, 16], ["not_implies", 21, 1], ["implied_by", 15, 20], ["or", 28, 19], ["not_implies", 30, 26], ["or", 10, 27], ["and", 4, 22], ["zero", 12, 25], ["not_b", 6, 15], ["or", 16, 14], ["not_or", 19, 6], ["not_implied_by", 28, 4], ["not_a", 8, 2], ["a", 7, 24], ["not_a", 24, 10], ["not_or", 12, 27]], [["not_implies", 27, 1], ["implied_by", 18, 6], ["b", 7, 23], ["b", 20, 25], ["and", 1, 23], ["not_b", 10, 9], ["b", 3, 21], ["zero", 30, 0], ["a", 15, 30], ["not_xor", 25, 0], ["one", 5, 12], ["b", 2, 12], ["not_or", 4, 14], ["implied_by", 13, 10], ["b", 29, 24], ["not_implied_by", 31, 3], ["not_b", 16, 31], ["implied_by", 27, 6], ["not_b", 19, 26], ["not_and", 26, 15], ["a", 24, 28], ["not_b", 11, 17], ["not_and", 28, 7], ["not_or", 2, 18], ["not_a", 21, 22], ["a", 8, 13], ["implies", 22, 14], ["not_implied_by", 4, 11], ["a", 17, 16], ["not_or", 19, 29], ["b", 8, 20], ["b", 5, 9]], [["a", 11, 27], ["not_a", 29, 7], ["not_and", 30, 30], ["a", 28, 20], ["b", 22, 6], ["not_and", 8, 5], ["a", 17, 4], ["or", 1, 19], ["a", 2, 11], ["one", 25, 8], ["not_and", 3, 13], ["xor", 4, 14], ["not_xor", 9, 12], ["implied_by", 28, 10], ["implied_by", 25, 9], ["not_implied_by", 16, 29], ["implied_by", 2, 26], ["or", 15, 24], ["not_xor", 1, 0], ["b", 10, 31], ["not_b", 18, 3], ["not_a", 7, 16], ["not_or", 12, 17], ["and", 18, 0], ["b", 23, 24], ["not_and", 19, 13], ["not_or", 23, 14], ["implies", 6, 27], ["a", 21, 15], ["and", 26, 20], ["not_implied_by", 22, 21], ["not_and", 31, 5]], [["b", 12, 21], ["b", 2, 1], ["not_xor", 29, 2], ["not_a", 3, 16], ["and", 22, 19], ["implied_by", 8, 23], ["or", 11, 18], ["b", 25, 5], ["not_a", 1, 7], ["or", 0, 27], ["or", 16, 17], ["not_b", 21, 24], ["one", 28, 3], ["not_and", 20, 27], ["implies", 9, 15], ["and", 10, 13], ["a", 18, 0], ["and", 15, 25], ["b", 31, 20], ["not_and", 19, 9], ["not_implies", 11, 28], ["implied_by", 6, 29], ["not_b", 12, 4], ["and", 17, 24], ["not_b", 10, 4], ["or", 7, 5], ["not_a", 26, 30], ["implies", 13, 8], ["b", 23, 6], ["not_a", 30, 31], ["implies", 22, 26], ["or", 14, 14]], [["not_a", 7, 0], ["not_implied_by", 22, 21], ["not_implies", 31, 3], ["not_b", 18, 24], ["not_or", 6, 29], ["b", 2, 1], ["xor", 19, 20], ["not_xor", 30, 11], ["not_and", 26, 14], ["and", 9, 8], ["not_and", 5, 4], ["or", 15, 27], ["implies", 25, 23], ["not_implied_by", 12, 16], ["implies", 13, 10], ["implies", 28, 17]], [["and", 10, 1], ["and", 7, 8], ["not_a", 12, 5], ["not_implied_by", 13, 3], ["not_or", 9, 15], ["not_implies", 6, 4], ["not_implied_by", 0, 2], ["not_b", 11, 14]]]
// 16-bit-input network:
let NETWORK: Network = [[["not_a", 2, 9], ["not_and", 6, 4], ["zero", 8, 14], ["b", 14, 11], ["zero", 0, 1], ["a", 15, 13], ["not_a", 6, 8], ["b", 10, 11], ["b", 7, 6], ["not_a", 6, 3], ["not_b", 10, 3], ["not_b", 14, 5], ["one", 3, 9], ["and", 11, 2], ["not_a", 5, 7], ["one", 9, 7], ["b", 7, 15], ["not_b", 9, 13], ["not_and", 1, 1], ["not_a", 5, 12], ["xor", 2, 4], ["one", 0, 8], ["a", 3, 0], ["not_b", 4, 4], ["not_b", 15, 8], ["a", 11, 5], ["a", 15, 0], ["one", 14, 12], ["not_a", 2, 12], ["a", 13, 10], ["a", 13, 1], ["one", 10, 12]], [["not_and", 9, 29], ["not_b", 1, 26], ["b", 4, 22], ["b", 31, 10], ["implied_by", 13, 26], ["b", 17, 28], ["not_a", 29, 2], ["not_b", 18, 16], ["not_a", 3, 12], ["not_and", 15, 2], ["a", 18, 15], ["not_implied_by", 21, 1], ["not_a", 25, 8], ["b", 10, 5], ["b", 31, 16], ["or", 19, 19], ["b", 21, 9], ["implied_by", 30, 17], ["not_implied_by", 11, 27], ["and", 20, 14], ["one", 14, 6], ["a", 3, 5], ["not_b", 7, 23], ["zero", 27, 0], ["a", 22, 20], ["implies", 0, 7], ["not_a", 30, 4], ["one", 24, 24], ["not_and", 8, 25], ["xor", 13, 23], ["xor", 11, 28], ["zero", 12, 6]], [["and", 12, 5], ["not_and", 7, 6], ["implies", 27, 17], ["not_implies", 2, 26], ["not_implied_by", 16, 0], ["not_b", 2, 18], ["not_and", 24, 1], ["a", 21, 30], ["not_implies", 15, 6], ["not_and", 14, 9], ["and", 31, 3], ["a", 19, 13], ["xor", 10, 28], ["zero", 23, 20], ["implied_by", 10, 14], ["not_or", 3, 26], ["or", 12, 28], ["not_implied_by", 1, 30], ["not_b", 13, 25], ["and", 18, 15], ["not_xor", 29, 11], ["not_implies", 7, 23], ["not_a", 22, 9], ["not_or", 29, 8], ["xor", 17, 11], ["not_a", 4, 0], ["implied_by", 25, 8], ["not_b", 31, 21], ["not_b", 20, 4], ["not_b", 5, 16], ["or", 19, 27], ["not_or", 22, 24]], [["not_implied_by", 6, 22], ["and", 20, 15], ["not_a", 12, 24], ["one", 5, 30], ["b", 6, 0], ["xor", 30, 27], ["b", 13, 8], ["b", 13, 26], ["b", 10, 16], ["or", 25, 7], ["not_or", 23, 11], ["not_implied_by", 4, 5], ["a", 21, 28], ["not_a", 14, 29], ["not_implies", 2, 19], ["not_implies", 1, 31], ["not_implied_by", 18, 4], ["not_a", 25, 10], ["not_and", 16, 9], ["not_a", 1, 7], ["implies", 2, 31], ["xor", 29, 17], ["not_or", 19, 18], ["not_b", 9, 3], ["b", 26, 0], ["implied_by", 8, 14], ["or", 21, 3], ["zero", 11, 17], ["xor", 27, 28], ["not_and", 20, 15], ["and", 23, 12], ["implies", 24, 22]], [["or", 31, 29], ["not_and", 7, 8], ["zero", 16, 21], ["implied_by", 17, 7], ["zero", 13, 12], ["not_implies", 1, 3], ["not_xor", 29, 21], ["not_implied_by", 10, 14], ["not_implies", 23, 26], ["not_a", 4, 30], ["not_a", 5, 0], ["and", 25, 9], ["implied_by", 6, 11], ["xor", 5, 28], ["implied_by", 23, 15], ["b", 13, 20], ["not_or", 2, 0], ["not_xor", 27, 1], ["and", 18, 16], ["not_or", 25, 31], ["not_a", 20, 14], ["and", 12, 11], ["not_xor", 8, 15], ["implies", 9, 4], ["not_and", 18, 24], ["xor", 3, 17], ["b", 27, 24], ["not_b", 22, 28], ["b", 22, 2], ["b", 30, 19], ["b", 6, 19], ["not_xor", 26, 10]], [["b", 4, 0], ["not_b", 12, 8], ["and", 18, 2], ["a", 16, 5], ["not_xor", 25, 28], ["not_and", 30, 13], ["not_xor", 17, 27], ["not_xor", 7, 11], ["not_and", 20, 9], ["zero", 31, 30], ["not_xor", 15, 23], ["not_implied_by", 26, 27], ["b", 1, 16], ["not_or", 12, 8], ["not_or", 20, 11], ["implied_by", 22, 21], ["not_implied_by", 19, 13], ["b", 6, 18], ["not_implied_by", 1, 14], ["a", 4, 10], ["implied_by", 29, 24], ["b", 3, 22], ["implied_by", 10, 6], ["not_and", 17, 25], ["not_implied_by", 0, 24], ["not_b", 2, 5], ["not_b", 31, 23], ["implied_by", 15, 21], ["not_a", 9, 26], ["not_b", 19, 3], ["a", 7, 29], ["not_xor", 28, 14]], [["not_or", 8, 14], ["not_b", 25, 29], ["not_implied_by", 17, 20], ["not_b", 19, 3], ["and", 26, 5], ["xor", 22, 0], ["implies", 31, 11], ["b", 2, 27], ["a", 6, 30], ["not_b", 7, 10], ["implied_by", 21, 16], ["not_a", 13, 9], ["a", 23, 28], ["b", 12, 15], ["or", 24, 18], ["not_xor", 4, 1]], [["b", 7, 14], ["not_xor", 0, 12], ["not_or", 3, 1], ["and", 6, 11], ["b", 4, 8], ["and", 10, 15], ["not_implied_by", 2, 5], ["and", 13, 9]]]

let num_inputs = Math.max(...NETWORK[0].map(n => [n[1], n[2]]).flat(1)) + 1
let num_gates = NETWORK.flat(1).length
console.log("network size", num_inputs, num_gates)

function sortGates(NETWORK: Network) {
    // TODO: Ideally would do something to minimize overall separation between children and parents (or perhaps minimize wire-crossing).
    // This is just a greedy heuristic: sort gates in each layer by index (in the previous layer) of their first operand (with index of second operand as tie-breaker).
    // NOTE: Can't rearrange last layer because order of output bits matters.
    for (let layerIndex = 0; layerIndex < NETWORK.length - 1; layerIndex++) {
        const layer = NETWORK[layerIndex]
        const indices = [...new Array(layer.length).keys()]
        // TODO: Take operation's use of operands into account.
        indices.sort((i, j) => (layer[i][1] - layer[j][1]) || (layer[i][2] - layer[j][2]))
        NETWORK[layerIndex] = indices.map(i => layer[i])
        // After rearranging the gates within a layer, we must also the update input indices of the next layer.
        NETWORK[layerIndex + 1] = NETWORK[layerIndex + 1].map(([op, a, b]) => [op, indices.indexOf(a), indices.indexOf(b)])
    }
}

sortGates(NETWORK)

type Mask = { [key: string]: "zero" | "one" }

function Network({
    network, mask, setMask, view, belaCursor
}: {
    network: Network, mask: Mask, setMask: (m: Mask) => void, view: View, belaCursor: string | null
}) {
    const [_, setCounts] = useState<null | Uint8Array>(null)
    const collapsed = view !== "full"
    const xSpacing = collapsed ? 25 : 75
    const ySpacing = 25
    const nodeWidth = 25
    const nodeHeight = 25

    const maskGate = (layerIndex: number, index: number, state: boolean) => {
        setMask({...mask, [JSON.stringify([layerIndex, index])]: state ? "one" : "zero"})
    }

    const touchMoveHandler = (e: React.TouchEvent) => {
        // This is necessary because there's no touch equivalent to `onMouseOver`.
        const el = document.elementFromPoint(e.touches[0].clientX, e.touches[0].clientY)
        if (el?.id.startsWith("gate")) {
            const [_, layerIndex, index] = el.id.split("-")
            maskGate(+layerIndex, +index, false)
        }
    }

    useEffect((() => {
        function loop() {
            setCounts(counts)
            handle = requestAnimationFrame(loop)
        }
        let handle = requestAnimationFrame(loop)
        return () => cancelAnimationFrame(handle)
    }))

    let gateIndex = num_inputs

    const maxLayerSize = Math.max(...network.map(l => l.length))
    const ratio = (view === "square") ? (ySpacing * maxLayerSize / (xSpacing * network.length)) : 1

    const layerOffset = collapsed ? 0 : 1
    return <svg height="1000" viewBox={`0 0 ${xSpacing * (network.length + layerOffset)} 1000`} transform={`scale(${ratio}, 1)`} onMouseUp={e => e.buttons === 0 && setMask({})} onContextMenu={e => e.preventDefault()}>
        {!collapsed && [...new Array(num_inputs)].map((_, index) => {
            const x = 0
            const y = index * ySpacing
            const bg = (counts?.[index] ?? 0) / 128 * 255
            const fg = bg < 128 ? 255 : 0
            return <g key={index}>
                <rect x={x} y={y} width={nodeWidth} height={nodeHeight} fill={`rgb(${bg}, ${bg}, ${bg})`} stroke="black"></rect>
                <text x={x+5} y={y+12} fontSize="10" style={{ userSelect: "none" }} fill={`rgb(${fg}, ${fg}, ${fg})`}>in{index}</text>
            </g>
        })}
        {network.map((layer, layerIndex) => {
            return layer.map(([op, a, b], index) => {
                const x = (layerIndex + layerOffset) * xSpacing
                const verticalScale = view === "square" ? maxLayerSize / layer.length : 1
                const y = index * ySpacing
                const coords = JSON.stringify([layerIndex, index])
                const masked = mask[coords]
                op = masked ?? op
                const selected = belaCursor === coords
                const unary = OP_INPUTS[op][0] !== OP_INPUTS[op][1]
                const bg = (counts?.[gateIndex++] ?? 0) / 128 * 255
                const fg = bg < 128 ? 255 : 0
                const Icon = () => (OP_ICONS as any)[op]
                const borderColor = selected ? "red" : (masked ? "cyan" : "black")
                const borderWidth = (selected || masked) ? 6 : 1
                // TODO: Support drawing masks on mobile.
                return <g key={index} transform={`scale(1, ${verticalScale}) translate(${x}, ${y})`}>
                    <rect
                        id={`gate-${layerIndex}-${index}`}
                        x={0} y={0} width={nodeWidth} height={nodeHeight} stroke={borderColor} strokeWidth={borderWidth}
                        onMouseDown={e => maskGate(layerIndex, index, !!(e.buttons & 1))}
                        onMouseOver={e => e.buttons && maskGate(layerIndex, index, !!(e.buttons & 1))}
                        onTouchStart={() => maskGate(layerIndex, index, false)}
                        onTouchMove={e => touchMoveHandler(e)}
                        fill={`rgb(${bg}, ${bg}, ${bg})`}
                        style={{ touchAction: "none" }}
                    ></rect>
                    {!collapsed && <>
                        {OP_INPUTS[op][0] && <line className="wire" x1={-xSpacing + nodeWidth} y1={a * ySpacing + nodeHeight / 2 - y} x2={0} y2={nodeHeight * (unary ? 1/2 : 4/11)} />}
                        {OP_INPUTS[op][1] && <line className="wire" x1={-xSpacing + nodeWidth} y1={b * ySpacing + nodeHeight / 2 - y} x2={0} y2={nodeHeight * (unary ? 1/2 : 7/11)} />}
                    </>}
                    <g transform={`translate(0, ${nodeHeight/2}) scale(${nodeWidth})`} strokeWidth="10" fill={`rgb(${fg}, ${fg}, ${fg})`} stroke={`rgb(${fg}, ${fg}, ${fg})`}>
                        <Icon />
                    </g>
                </g>
            })
        })}
    </svg>
}

function applyMask(network: Network, mask: Mask): Network {
    return network.map((layer, layerIndex) => layer.map(([op, a, b], index) => {
        return [mask[JSON.stringify([layerIndex, index])] ?? op, a, b]
    }))
}

type View = "full" | "compact" | "square"

let fromBela = false
let sendNetwork: (network: Network, _belaData?: BelaData | null) => void // HACK for react -_-

function App() {
    const [network, _setNetwork] = useState(NETWORK)
    const [mask, _setMask] = useState({})
    const [view, setView] = useState<View>("full")
    const [belaData, setBelaData] = useState<BelaData | null>(null)
    const [belaCursor, setBelaCursor] = useState<string | null>(null)

    sendNetwork = (network: Network, _belaData: BelaData | null = belaData) => {
        console.log("sendNetwork", fromBela)
        node.port.postMessage({ cmd: "loadNetwork", network })
    
        if (!fromBela && _belaData !== null) {
            const numInputs = Math.max(...network[0].map(n => [n[1], n[2]]).flat(1)) + 1
            const data = [numInputs]
            for (const layer of network) {
                data.push(layer.length)
                for (const [op, a, b] of layer) {
                    data.push(OPS.indexOf(op), a, b)
                }
            }
            console.log("sending to bela", data.length)
            _belaData.sendBuffer(0, "int", data)
        }
    }

    const setNetwork = (network: Network) => {
        _setNetwork(network)
        sendNetwork(network)
    }

    const setMask = (mask: Mask) => {
        _setMask(mask)
        sendNetwork(applyMask(network, mask))
    }

    const randomize = async () => {
        let lastLength = num_inputs
        const network = NETWORK.map(layer => {
            const newLayer = layer.map(_ => {
                const op = OPS[~~(Math.random() * OPS.length)]
                const a = ~~(Math.random() * lastLength)
                const b = ~~(Math.random() * lastLength)
                return [op, a, b] as [Operation, number, number]
            })
            lastLength = layer.length
            return newLayer
        })
        sortGates(network)
        setNetwork(network)
    }

    const loadNetwork = (s: string) => {
        const network = JSON.parse(s)
        sortGates(network)
        setNetwork(network)
    }

    const resetTime = () => {
        node.port.postMessage({ cmd: "resetTime" }) // TODO Send to Bela
    }

    const toggleBela = (useBela: boolean) => {
        if (useBela) {
            const belaData = new BelaData(5555, "gui_data", "bela.local")
            setBelaData(belaData)
            sendNetwork(network, belaData)
            belaData.target.addEventListener("buffer-ready", (e: CustomEvent) => {
                if (e.detail === 1) {
                    console.log(e)
                    // Action
                    // TODO: do this on the Bela side
                    randomize()
                } else if (belaData.buffers[0][0] === -1) {
                    // Touch released
                    setBelaCursor(null)
                } else {
                    const coords = JSON.stringify(belaData.buffers[0])
                    setBelaCursor(coords)
                }
            })
        } else {
            belaData.ws.close() // TODO: prevent automatic reconnection
            setBelaData(null)
        }
    }

    useEffect(() => {
        if (!node) return
        // Would just do this in the event listener setup in `toggleBela`,
        // but then `mask` is a stale captured binding (yay react...).
        fromBela = true  // HACK: don't send updates *from* Bela back *to* Bela
        if (belaCursor === null) {
            setMask({})
        } else {
            setMask({...mask, [belaCursor]: "zero"})
        }
        fromBela = false
    }, [belaCursor])

    const [volume, setVolume] = useState(50)

    useEffect(() => {
        const gain = (volume === 0) ? 0 : 10**((volume / 100 - 1) * 64 / 20)
        if (belaData !== null) {
            belaData.sendBuffer(1, "float", [gain])
        }
        gainNode.gain.value = gain
    }, [volume])

    return <div style={{ display: "flex", flexDirection: "column" }}>
        <div style={{ marginBottom: "0.5em" }}>
            <strong>glitch gate</strong>. hint: click once to start playing. drag to draw masks (left = 1, right = 0). try randomizing.
        </div>
        <input type="text" value={JSON.stringify(network)} onChange={e => loadNetwork(e.target.value)} style={{ fontFamily: "monospace" }} />
        <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
            <button onClick={resetTime}>Reset Time</button>
            <button onClick={randomize}>Randomize</button>
            <label style={{ margin: "0 1em" }}>
                View
                <select value={view} onChange={e => setView(e.target.value as View)} style={{ margin: "0 0.5em" }}>
                    <option value="full">Full</option>
                    <option value="compact">Compact</option>
                    <option value="square">Square</option>
                </select>
            </label>
            <label>Use Bela<input type="checkbox" checked={belaData !== null} onChange={e => toggleBela(e.target.checked)} /></label>
            <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
            <label style={{ display: "flex", alignItems: "center", margin: "0 1em" }}>
                Volume
                <input type="range" min="0" max="100" value={volume} onChange={e => setVolume(+e.target.value)} />
            </label>
        </div>
        </div>
        <Network network={network} mask={mask} setMask={setMask} view={view} belaCursor={belaCursor} />
    </div>
}

export default App
