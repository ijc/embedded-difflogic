#include <Bela.h>
#include <libraries/Trill/Trill.h>
#include <libraries/Gui/Gui.h>

#include <iostream>
#include <cmath>
#include <string.h>
#include <assert.h>
#include <vector>

// HACK
namespace Interpreter {
	#include "../interpreter.c"
}

struct Node {
	int op;  // gate operation
	int input_a;  // index of first operand in previous layer
	int input_b;  // index of second operand in previous layer
	int index;  // index of node in flattened network
};

std::vector<std::vector<Node>> network;

const int num_outputs = 8;

float t = 0.0f;
float rate;
float gain = 0.5;

Gui gui;
Trill touchSensor;
float touchPosition[2] = { 0.0 , 0.0 };
float touchSize = 0.0;

// TODO: implement way to load in a network without relying on input from browser over websocket
// (e.g. randomize button, or default network)
bool receiveData(const char *buffer, unsigned int size, void *data) {
	// NOTE: Annoyingly, Bela GUI library spams console with messages like "Received buffer ID 0 is out of range";
	// these can be ignored because we are handling the messages ourselves (rather than relying on the default deserialization & storage behavior).

	std::cout << "received data! " << size << std::endl;
	int *ints = (int *)buffer;
	// First four ints are header fields (part of the Bela GUI library).
	int id = ints[0];
	if (id == 1) {
		// Set gain
		gain = ((float *)buffer)[4];
		return true;
	}
	assert(id == 0);
	// Next int is the number of inputs to the network.
	Interpreter::num_inputs = ints[4];
	// Following this, we have:
	// - number of nodes in layer (one int)
	// - op, input_a, input_b (three ints per node)
	// Reconstruct network layers on this side, and convert to flat format expected by the interpreter.
	network.clear();
	int layer_index = 0;
	int num_gates = 0;
	int offset = 0;
	int last_layer_size = Interpreter::num_inputs; // TODO: technically could figure this out from range of input indices to first layer.
	fprintf(stderr, "number of inputs: %d\n", Interpreter::num_inputs);
	for (int i = 5; i < size / sizeof(int); ) {
		int layer_size = ints[i++];
		fprintf(stderr, "layer %d: %d nodes\n", layer_index++, layer_size);
		network.emplace_back();
		for (int j = 0; j < layer_size; j++) {
			int op = ints[i++];
			int input_a = ints[i++];
			int input_b = ints[i++];
			network.back().push_back({op, input_a, input_b, num_gates});
			Interpreter::network[num_gates++] = {input_a + offset, input_b + offset, op};
		}
		offset += last_layer_size;
		last_layer_size = layer_size;
	}
	Interpreter::num_gates = num_gates;
	return true;
}

bool touchedRandomize = false;

void trillLoop(void*) {
	while (!Bela_stopRequested()) {
		touchSensor.readI2C();
		touchSize = touchSensor.compoundTouchSize();
		touchPosition[0] = touchSensor.compoundTouchHorizontalLocation();
		touchPosition[1] = touchSensor.compoundTouchLocation();
		if (touchSize > 0.0f) {
			fprintf(stderr, "touch: %f @ (%f, %f)\n", touchSize, touchPosition[0], touchPosition[1]);
			const float buttonOffset = 0.1f;
			if (touchPosition[0] < buttonOffset) {
				fprintf(stderr, "TODO randomize\n");
				// TODO do this locally, then send updated network over websocket
				// for (int layerIndex = 0; layerIndex < network.size(); layerIndex++) {
				// 	for (auto &gate : network[layerIndex]) {
				// 		gate.op = rand() % 16;
				// 		int prevOutputs = layerIndex == 0 ? Interpreter::num_inputs : network[layerIndex - 1].size();
				// 		gate.input_a = rand() % prevOutputs;
				// 		gate.input_b = rand() % prevOutputs;
				// 	}
				// }
				if (!touchedRandomize) {
					int action[] = {0};
					gui.sendBuffer(1, action);
					touchedRandomize = true;
				}
			} else if (!network.empty()) {
				touchPosition[0] = (touchPosition[0] - buttonOffset) / (1.0f - buttonOffset);
				// Determine corresponding layer + node in the network
				int layerIndex = min(touchPosition[0] * network.size(), network.size() - 1);
				int gateIndex = min((1.0f - touchPosition[1]) * network[layerIndex].size(), network[layerIndex].size() - 1);
				fprintf(stderr, "layer %d gate %d\n", layerIndex, gateIndex);
				// Replace selected gate with constant (temporary mask)
				Interpreter::network[network[layerIndex][gateIndex].index].op = Interpreter::OPERATIONS::ZERO;
				int coords[] = {layerIndex, gateIndex};
				gui.sendBuffer(0, coords);
			}
		} else {
			touchedRandomize = false;
			// Restore network on touch release.
			// TODO: deduplicate with `receiveData()`?
			// TODO: only need to do this once after release
			int offset = 0;
			int last_layer_size = Interpreter::num_inputs;
			for (auto &layer : network) {
				for (auto &gate : layer) {
					Interpreter::network[gate.index] = {gate.input_a + offset, gate.input_b + offset, gate.op};
				}
				offset += last_layer_size;
				last_layer_size = layer.size();
			}
			int coords[] = {-1, -1};
			gui.sendBuffer(0, coords);
		}
		usleep(12000); // microseconds
	}
}

bool setup(BelaContext *context, void *userData) {
	// Setup a Trill Square on I2C bus 1, using the default mode and address
	if (touchSensor.setup(1, Trill::SQUARE) == 0) {
		fprintf(stderr, "Connected to Trill Square\n");
		touchSensor.printDetails();
		Bela_runAuxiliaryTask(trillLoop);
	} else {
		fprintf(stderr, "Unable to initialize Trill Square\n");
		return false;
	}

	rate = 8000.0 / context->audioSampleRate;
	// rate = 8669.8666667 / context->audioSampleRate;
    gui.setup(context->projectName);
    //gui.setBuffer('f', 3);
    gui.setBinaryDataCallback(receiveData);

	return true;
}

void render(BelaContext *context, void *userData) {
    int period = 1 << Interpreter::num_inputs;

	for (unsigned int n = 0; n < context->audioFrames; n++) {
		// Setup input.
		int ti = t; // ZOH resampling
		// TODO: process in batches
		for (int i = 0; i < Interpreter::num_inputs; i++) {
			Interpreter::data[i] = (ti >> i) & 1;
		}

		// Run current version of network.
		Interpreter::logic_gate_net();
		
		t += rate;
		while (t > period) t -= period;

		// Extract output.
		int sample = 0;
		for (int i = 0; i < num_outputs; i++) {
			sample |= (Interpreter::data[Interpreter::num_inputs + Interpreter::num_gates - num_outputs + i] & 1) << i;
		}
		float out = (float)sample / ((1 << num_outputs) - 1) * 2 - 1;
		out *= gain;

		for (unsigned int channel = 0; channel < context->audioOutChannels; channel++) {
			audioWrite(context, n, channel, out);
		}
	}
}

void cleanup(BelaContext *context, void *userData) {
}