# embedded-difflogic

This repository contains artifacts from a 2024 Google Summer of Code project, _Differentiable Logic for Interactive Systems and Generative Music_.

Links:
- [Project Proposal](https://gsoc.beagleboard.io/proposals/ijc.html)
- [Update Thread](https://forum.beagleboard.org/t/weekly-progress-report-differentiable-logic-for-interactive-systems-and-generative-music/38486)
- [Final Report](https://ijc8.me/2024/08/26/gsoc-difflogic/)

# Organization

As of the end of GSoC'24, this repository is a bit of a mess, containing the accumulated debris of several phases of the project. I intend to spruce this up soon™ (and perhaps spin-off some parts of this project into separate repositories), but in the meantime, here's a brief guide to what stuff is:
- `explorer/` - interactive application for exploring the space of synthesis networks & manipulating them. takes the form of a web app (Vite, React, Web Audio, WebAssembly). can be used stand-alone or in conjunction with a Bela via WebSockets.
  - `explorer/bela/` - Bela project: running networks, playing output, reading from Trill Square, (optionally) communicating with web app
- `faust/` - minimal example of using a difflogic network (exported to C) from the [Faust](https://faust.grame.fr/) audio programming language via its FFI.
- `models/` - some small synthesis models saved in various forms. `.pkl` files includes the raw model (pre-discretization) pickled directly from Python; `.c` contains exported C source from difflogic's `CompiledLogicNet` (or customized variants); `.wasm` contains that exported C compiled to WebAssembly.
- `sc-difflogic/` - SuperCollider UGen for running a difflogic network in the audio graph
- `test-pru/` - Bela project that runs a difflogic network on the PRU
- `assembled_model.py` - alternative exporter based on difflogic's `CompiledLogicNet`, adapted for generating PRU assembly
  - see `graph.ipynb` for more complete version
- `compiled_model.py` - tweaked version of `CompiledLogicNet` adapted to support networks without `GroupSum` (as in our synthesis networks)
- `difflogic-wrapper.cpp` - example of using a difflogic network from C++ (with a more C++-ish façade)
- `pd_difflogic~.c` - Pure Data external that provides `difflogic` and `difflogic~` objects
  - `test.pd`, `_main.pd` - example patches that use the external on desktop & Bela, respectively
- `build-pd.sh` - build script for Pure Data external
- `generate_network_pasm.sh` - cursed build script from when I was still trying to wrangle various disparate PRU toolchains (before just generating PRU assembly directly from Python)
- `graph.py` - utility functions for converting difflogic network to networkx graph and drawing them
- `utils.py` - utility functions for converting input & output to bits for difflogic networks, using various encoding schemes, and for training & testing models
- `intro.ipynb` - trivial example of creating a difflogic network and exporting to C
- `identity.ipynb` - basic experiments with difflogic networks & input/output representations
- `identity_cuda.ipynb` - ditto but in a Google Colab notebook with a GPU
- `graph.ipynb` - experiments in simplying difflogic networks, culminating in generation of (optimized) PRU assembly
- `bytebeat.ipynb` - experiments with "bytebeat-style" (generating samples as a function of time) logic gate networks for audio synthesis; precursor to `explorer/`
- `example_scorecard.c`, `example_scorecard.wasm` - example of baking a synthesis network into a [ScoreCard](https://ijc8.me/s)
